import React from 'react';
import {Text, TextInput, View, TouchableOpacity} from 'react-native';
import styles from './styles';
import {getRegister} from './../../Redux/Actions/RegisterAction';
import {connect} from 'react-redux'

class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      valueInputFullname: '',
      valueInputEmail: '',
      valueInputPassword: '',
      isLoading: false,
      error: null,
    };
  }

  onChangeFullname = inputFullname => {
    this.setState({valueInputFullname: inputFullname});
  };

  onChangeEmail = inputEmail => {
    this.setState({valueInputEmail: inputEmail});
  };

  onChangePassword = inputPassword => {
    this.setState({valueInputPassword: inputPassword});
  };

  handleButtonSubmit = async () => {
    const dataUser = {
      email:this.state.valueInputEmail,
      password:this.state.valueInputPassword,
      name:this.state.valueInputFullname
    }
    this.props.getRegister(dataUser, userRegister => {
      console.log(userRegister, 'Tes print register');

      if (userRegister.isRegisterSuccess === true)
        {this.props.navigation.replace('SuccessRegister');}
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Peepers</Text>
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Full Name"
            placeholderTextColor="white"
            onChangeText={this.onChangeFullname}/>
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Email"
            placeholderTextColor="white"
            onChangeText={this.onChangeEmail}/>
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password"
            placeholderTextColor="white"
            onChangeText={this.onChangePassword}/>
        </View>
        <TouchableOpacity
          style={styles.loginBtn}
          title={this.state.isLoading === true ? 'Loading...' : 'Kirim Data'}
          onPress={this.handleButtonSubmit}
          disabled={this.state.isLoading === true ? true : false}>
          <Text style={styles.loginText}>Register</Text>
        </TouchableOpacity>
        <TouchableOpacity 
            onPress={()=>{
                this.props.navigation.navigate('Login', {});
            }}>
          <Text style={styles.registerText}>Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
}



const mapStateToProps = state =>{
  return {
    // user:state.LoginReducer,  //Register_A_6
    userRegister:state.LoginReducer.userRegister, //Register_A_6
  }
}

const mapDispatchToProps = {
  getRegister:getRegister,
}

export default connect(mapStateToProps,mapDispatchToProps)(Register);
