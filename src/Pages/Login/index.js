import React from 'react';
import {Text, TextInput, View, TouchableOpacity} from 'react-native';
import styles from './styles';
import {getLogin} from './../../Redux/Actions/LoginAction'
import {connect} from 'react-redux'

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      valueInputEmail: '',
      valueInputPassword: '',
      isLoading: false,
      error: null,
    };
  }

  onChangeEmail = inputEmail => {
    this.setState({valueInputEmail: inputEmail});
  };

  onChangePassword = inputPassword => {
    this.setState({valueInputPassword: inputPassword});
  };

  handleButtonSubmit = () => {
    const dataUser = {
      email:this.state.valueInputEmail,
      password:this.state.valueInputPassword
    }
    this.props.getLogin(dataUser, user => {
      if (user.isLoginSuccess === true)
        {this.props.navigation.replace('MainApp');}
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Peepers</Text>
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Email..."
            placeholderTextColor="white"
            onChangeText={this.onChangeEmail}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password..."
            placeholderTextColor="white"
            onChangeText={this.onChangePassword}
          />
        </View>
        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.loginBtn}
          title={this.state.isLoading === true ? 'Loading...' : 'Kirim Data'}
          onPress={this.handleButtonSubmit}
          disabled={this.state.isLoading === true ? true : false}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          onPress={()=>{
            this.props.navigation.replace('Register');
          }}>
          <Text style={styles.registerText}>Register</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state =>{
  return {
    user:state.LoginReducer.user, 
  }
}

const mapDispatchToProps = {
  getLogin:getLogin,
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);
