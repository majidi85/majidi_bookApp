import Splash from "./Splash";
import Login from "./Login";
import Register from "./Register"
import SuccessRegister from "./SuccessRegister";
import Home from "./Home"
import BookDetail from "./BookDetail"
import SearchTab from "./SearchTab"
import FavoritTab from "./FavoritTab"


export {Splash, Login, Register,SuccessRegister, Home,BookDetail,SearchTab,FavoritTab};