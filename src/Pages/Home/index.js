import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  RefreshControl,
} from 'react-native';
import styles from './styles';
import {connect} from 'react-redux';
import {getBookList} from '../../Redux/Actions/HomeAction';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/Ionicons';
import Skeleton from './skeleton';

class Home extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.props.getBookList();
  }

  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.props.loading}
            onRefresh={() => this.props.getBookList()}
          />
        }>
        {this.props.loading ? (
          <Skeleton />
        ) : (
          <>
            <View style={styles.header}>
              <Text style={styles.welcome}>
                Welcome, {this.props.dataUser.name}!
              </Text>
            </View>
            <View style={styles.body}>
              <Text style={styles.textRecommended}>Recommended Book</Text>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {this.props.dataBook
                  // .sort((a, b) => (a.average_rating < b.average_rating ? 1 : -1))
                  .sort((a, b) => b.average_rating - a.average_rating)
                  .map(book => (
                    <TouchableOpacity
                      key={book.id}
                      onPress={() => {
                        this.props.navigation.navigate('BookDetail', {
                          bookID: book.id,
                        });
                      }}>
                      <View key={book.id} style={styles.listBook}>
                        <Image
                          style={styles.imgBook}
                          source={{uri: `${book.cover_image}`}}
                        />
                        <Text style={styles.judul}>{book.title}</Text>
                        <Text style={styles.author}>{book.author}</Text>
                        <Stars
                          rating={book.average_rating / 2}
                          count={5}
                          half={true}
                          fullStar={
                            <Icon name="star" size={20} color="orange" />
                          }
                          emptyStar={
                            <Icon
                              name="star-outline"
                              size={20}
                              color="orange"
                            />
                          }
                          halfStar={
                            <Icon
                              name="star-half-outline"
                              size={20}
                              color="orange"
                            />
                          }
                        />
                      </View>
                    </TouchableOpacity>
                  ))}
              </ScrollView>
            </View>
            <View style={styles.footer}>
              <Text style={styles.textRecommended}>Popular Book</Text>
              <View>
                {this.props.dataBook
                  .sort((a, b) => (a.id > b.id ? -1 : 1))
                  .map(book => (
                    <TouchableOpacity
                      key={book.id}
                      style={styles.popular}
                      onPress={() => {
                        this.props.navigation.navigate('BookDetail', {
                          bookID: book.id,
                        });
                      }}>
                      <View>
                        <Image
                          style={styles.imgBook}
                          source={{uri: `${book.cover_image}`}}
                        />
                      </View>
                      <View style={styles.descript}>
                        <Text style={styles.judul}>{book.title}</Text>
                        <Text style={styles.author}>{book.author}</Text>
                        <Stars
                          rating={book.average_rating / 2}
                          count={5}
                          half={true}
                          fullStar={
                            <Icon name="star" size={20} color="orange" />
                          }
                          emptyStar={
                            <Icon
                              name="star-outline"
                              size={20}
                              color="orange"
                            />
                          }
                          halfStar={
                            <Icon
                              name="star-half-outline"
                              size={20}
                              color="orange"
                            />
                          }
                        />
                        <Text style={styles.author}>
                          Price :{' '}
                          {parseInt(book.price).toLocaleString('id-ID', {
                            currency: 'IDR',
                            style: 'currency',
                          })}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  ))}
              </View>
            </View>
          </>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataBook: state.HomeReducer.listBook,
    dataUser: state.LoginReducer.user,
    loading: state.HomeReducer.isLoading,
  };
};

const mapDispatchToProps = {
  getBookList: getBookList,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
