import React, {Component} from 'react';
import {Text, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export class Skeleton extends Component {
  render() {
    return (
      <View>
        <SkeletonPlaceholder>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
            <View style={{width: 30, height: 30, borderRadius: 15}} />
            <View
              style={{width: 120, height: 20, borderRadius: 4, marginLeft: 6}}
            />
            <View
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                marginLeft: 200,
              }}
            />
          </View>
          <View style={{marginTop: 15}}>
            <View
              style={{
                width: 150,
                height: 30,
                borderRadius: 4,
                marginLeft: 10,
                marginBottom: 10,
              }}
            />
            <View style={{flexDirection: 'row'}}>
              <View
                style={{
                  width: 108,
                  height: 192,
                  borderRadius: 4,
                  marginLeft: 10,
                }}
              />
              <View
                style={{
                  width: 108,
                  height: 192,
                  borderRadius: 4,
                  marginLeft: 20,
                }}
              />
              <View
                style={{
                  width: 108,
                  height: 192,
                  borderRadius: 4,
                  marginLeft: 20,
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 25}}>
            <View
              style={{
                width: 150,
                height: 30,
                borderRadius: 4,
                marginLeft: 10,
                marginBottom: 10,
              }}
            />
            <View>
              <View 
                style={{
                  flexDirection: 'row',
                  marginTop: 5,
                  }}>
                <View
                  style={{
                    width: 108,
                    height: 160,
                    borderRadius: 4,
                    marginLeft: 20,
                  }}
                />
                <View
                  style={{
                    marginTop: 2,
                    width: 108,
                    height: 30,
                    borderRadius: 4,
                    marginLeft: 20,
                  }}
                />
              </View>
              <View 
                style={{
                  flexDirection: 'row',
                  marginTop:5,
                  }}>
                <View
                  style={{
                    width: 108,
                    height: 160,
                    borderRadius: 4,
                    marginLeft: 20,
                  }}
                />
                <View
                  style={{
                    marginTop: 2,
                    width: 108,
                    height: 30,
                    borderRadius: 4,
                    marginLeft: 20,
                  }}
                />
              </View>
            </View>
          </View>
        </SkeletonPlaceholder>
      </View>
    );
  }
}

export default Skeleton;
