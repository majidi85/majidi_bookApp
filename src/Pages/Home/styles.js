import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
    container:{
        flex:1
    },
    header:{
        flex:1,
        justifyContent:'center',
        marginTop:10,
        marginLeft:10,
    },
    welcome:{
        fontSize:14,
        fontWeight:'bold',
        fontFamily:'Poppins'
    },
    body:{
        flex:12,
        marginBottom:10
    },
    textRecommended:{
        fontFamily: 'Poppins',
        fontWeight: 'bold',
        fontSize: 22,
        marginLeft: 15,
        color:'#06070D',
        marginTop:-5
    },
    imgBook:{
        height: 180,
        width: 123,
        borderRadius: 15,
        borderColor: 'grey',
        overflow: 'hidden',
        margin: 5,
        resizeMode: 'contain',
    },
    listBook:{
        marginLeft:10,
        height:250,
        width:130,
        alignItems:'center',
        justifyContent:'center',
        marginBottom:5
    },
    judul:{
        fontSize:16,
        fontWeight:'bold',
        fontFamily:'Poppins' 
     },
     author:{
         fontSize:14,
         fontFamily:'Poppins',
         fontWeight:'800'
     },
     rating:{
         fontSize:10
     },
    footer:{
        flex:6,
        margin:5
    },
    popular:{
        flexDirection:'row',
        flex:1,
        borderRadius: 15,
        margin:5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.19,
        shadowRadius: 4.65,
    },
    descript:{
        alignItems:'flex-start',
        justifyContent:'center',
        margin:5,
        padding:5,
        flex:1
    },

})

export default styles