import { StyleSheet } from "react-native";

const styles=StyleSheet.create ({
    container:{
        flex:1
    },
    top:{
        flex:1
    },
    topContainer:{
        flex:1,
    },
    atas:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        margin:10,
    },
    imgBook:{
        height:310,
        width:212,
        borderRadius:10,
    },
    judul:{
        fontFamily:'Poppins',
        fontSize:24,
        fontWeight:'bold',
        color:'#06070D'
    },
    author:{
        fontFamily:'Poppins',
        fontSize:15,
        fontWeight:'bold',
        marginLeft:5,
        marginRight:5
    },
    sinopsis:{
        fontFamily:'Poppins',
        fontSize:14,
        fontWeight:'bold',
        marginLeft:5,
        marginRight:5,
        marginTop:5,
    },
    sinopsises:{
        fontFamily:'Poppins',
        fontSize:14,
        fontWeight:'900',
        marginLeft:5,
        marginRight:5,
    },
    tengah:{
        flex:1,
        flexDirection:'row',
        marginLeft:15,
        justifyContent:'center'
    },
    bawah:{
        flex:1,
        margin:10,
    },
    
    viewIcon:{
        flexDirection:'row',
        margin:10,
    },
    iconLeft:{
        flex: 1,
        marginLeft:10,
    },
    iconRight:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    icon:{
        fontSize: 30,
        color: '#5364f2',
        marginLeft: 5,
        marginRight: 5,
    },
    heartBefore:{
        fontSize: 30,
        color: '#5364f2',
        marginLeft: 5,
        marginRight: 5,
    },
    heartAfter:{
        fontSize: 30,
        color: '#fc0335',
        marginLeft: 5,
        marginRight: 5,
    },
})

export default styles
