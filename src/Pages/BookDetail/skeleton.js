import React, {Component} from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export class Skeleton extends Component {
  render() {
    return (
      <View>
        <SkeletonPlaceholder>
          <View style={{flexDirection: 'row', marginTop: 20}}>
            <View style={{flex: 6, width: 30, height: 30, borderRadius: 15}} />
            <View
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                marginLeft: 20,
                flex: 1,
              }}
            />
            <View
              style={{
                flex: 1,
                width: 30,
                height: 30,
                borderRadius: 15,
              }}
            />
          </View>
          <View
            style={{
              marginTop: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 212,
                height: 310,
                borderRadius: 4,
                justifyContent: 'center',
                marginBottom: 10,
              }}
            />
            <View>
              <View
                style={{
                  width: 200,
                  height: 20,
                  borderRadius: 4,
                  justifyContent: 'center',
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  width: 100,
                  height: 20,
                  borderRadius: 4,
                  marginTop: 5,
                }}
              />
            </View>
            <View 
                style={{
                    marginTop: 15,
                    alignSelf:'baseline'
                }}>
              <View
                style={{
                  width: 80,
                  height: 20,
                  borderRadius: 4,
                  marginTop: 5,
                  marginLeft: 10,
                  alignItems:'flex-start'
                }}/>
                <View
                    style={{
                        width: 300,
                        height: 20,
                        borderRadius: 4,
                        marginTop: 5,
                        marginLeft: 10,
                        alignItems:'flex-start'
                }}/>
                <View
                    style={{
                        width: 300,
                        height: 20,
                        borderRadius: 4,
                        marginTop: 5,
                        marginLeft: 10,
                        alignItems:'flex-start'
                }}/>
                <View
                    style={{
                        width: 300,
                        height: 20,
                        borderRadius: 4,
                        marginTop: 5,
                        marginLeft: 10,
                        alignItems:'flex-start'
                }}/>
                <View
                    style={{
                        width: 300,
                        height: 20,
                        borderRadius: 4,
                        marginTop: 5,
                        marginLeft: 10,
                        alignItems:'flex-start'
                }}/>
            </View>
          </View>
        </SkeletonPlaceholder>
      </View>
    );
  }
}

export default Skeleton;
