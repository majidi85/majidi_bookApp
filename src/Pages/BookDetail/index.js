import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {connect} from 'react-redux';
import {getBookDetail} from '../../Redux/Actions/DetailAction';
import Share from 'react-native-share';
import {getFavorite, postFavorite} from '../../Redux/Actions/FavoriteAction';
import Stars from 'react-native-stars';
import {myNotification} from '../../Component/Notification';
import Skeleton from './skeleton';

class BookDetail extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.props.getBookDetail(this.props.route.params.bookID);
    this.props.getFavorite(this.props.route.params.bookID);
  }

  myCustomShare = async () => {
    const shareOptions = {
      message: `Buy & Read ${this.props.bookDetail.title} now at Peepers! See the book :  ${this.props.bookDetail.cover_image}`,
    };
    try {
      const ShareResponse = await Share.open(shareOptions);
      console.log(JSON.stringify(ShareResponse));
    } catch (error) {
      console.log('Error from sharing', error);
    }
  };

  handleFavorite = () => {
    const add = this.props.bookDetail.id;
    this.props.postFavorite(add);
    myNotification.configure();
    myNotification.createChannel('1');
    myNotification.notifactionHandler(
      '1',
      'Peepers!',
      `Success add ${this.props.bookDetail.title} to Favorite`,
    );
  };

  render() {
    const star = parseInt(this.props.bookDetail.average_rating);
    const stars = star / 2;
    const priceBook = parseInt(this.props.bookDetail.price);
    return (
      <ScrollView
        style={styles.container}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={this.props.loading}
            onRefresh={() =>
              this.props.getBookDetail(this.props.route.params.bookID)
            }
          />
        }>
        {this.props.loading ? (
          <Skeleton />
        ) : (
          <>
            <View style={styles.top}>
              <View style={styles.viewIcon}>
                <View style={styles.iconLeft}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.replace('MainApp');
                    }}>
                    <Icon name="arrow-back-circle" style={styles.icon} />
                  </TouchableOpacity>
                </View>
                <View style={styles.iconRight}>
                  {this.props.bookFavorite.some(
                    temp => temp._id === this.props.bookDetail.id,
                  ) ? (
                    <Icon name="heart-circle" style={styles.heartBefore} />
                  ) : (
                    <TouchableOpacity onPress={this.handleFavorite}>
                      <Icon name="heart-sharp" style={styles.heartAfter} />
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity onPress={this.myCustomShare}>
                    <Icon name="share-social-sharp" style={styles.icon} />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.topContainer}>
                <View style={styles.atas}>
                  <Image
                    source={{uri: `${this.props.bookDetail.cover_image}`}}
                    style={styles.imgBook}
                  />
                  <Text style={styles.judul}>
                    {this.props.bookDetail.title}
                  </Text>
                  <Text style={styles.author}>
                    {this.props.bookDetail.author}
                  </Text>
                  <Stars
                    rating={stars}
                    count={5}
                    half={true}
                    fullStar={<Icon name="star" size={20} color="orange" />}
                    emptyStar={
                      <Icon name="star-outline" size={20} color="orange" />
                    }
                    halfStar={
                      <Icon name="star-half-outline" size={20} color="orange" />
                    }
                  />
                  <Text style={styles.author}>
                    Publisher : {this.props.bookDetail.publisher}
                  </Text>
                </View>
                <View style={styles.tengah}>
                  <Text style={styles.author}>
                    Total sales : {this.props.bookDetail.total_sale}
                  </Text>
                  <Text style={styles.author}>
                    Stocks : {this.props.bookDetail.stock_available}
                  </Text>
                  <Text style={styles.author}>
                    Price :{' '}
                    {priceBook.toLocaleString('id-ID', {
                      currency: 'IDR',
                      style: 'currency',
                    })}
                  </Text>
                </View>
                <View style={styles.bawah}>
                  <Text style={styles.sinopsis}>
                    Pages : {this.props.bookDetail.page_count}
                  </Text>
                  <Text style={styles.sinopsis}>Synopsis : </Text>
                  <Text style={styles.sinopsises}>
                    {this.props.bookDetail.synopsis}
                  </Text>
                </View>
              </View>
            </View>
          </>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    bookDetail: state.DetailReducer.bookDetail, //Detail_A_6
    bookFavorite: state.FavoriteReducer.listFavorites,
    loading: state.DetailReducer.isLoading,
  };
};

const mapDispatchToProps = {
  getBookDetail: getBookDetail,
  postFavorite: postFavorite,
  getFavorite,
};

export default connect(mapStateToProps, mapDispatchToProps)(BookDetail);
