import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    namaApp:{
      flex:1,
      marginTop:50,
      marginBottom:10
    },
    textNamaApp:{
      fontWeight:"bold",
        fontSize:50,
        color:"#fb5b5a",
    },
    viewBG:{
      flex:8,
    },
    imgBG: {
      flex:1,
      width: 340, //408,
      height: 360, //444,
    },
    textmajidi:{
      justifyContent:'center',
      flex:1,
      marginBottom:30,
      marginTop:20
    },
    majidi:{
      justifyContent:'center'
    }
  });

  export default styles