import React, {useEffect} from 'react';
import {View,Image,Text} from 'react-native';
import { Majidi } from '../../Assets/Images';
import {ImageSplash} from '../../Assets/Images';

import styles from './styles';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Login');
    }, 3000);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <View style={styles.namaApp}>
        <Text style={styles.textNamaApp}>Peepers</Text>
      </View>
      <View style={styles.viewBG}>
        <Image source={ImageSplash} style={styles.imgBG} />
      </View>
      <View style={styles.textmajidi}>
          <Image source={Majidi} style={styles.majidi} />
      </View>
    </View>
  );
};

export default Splash;
