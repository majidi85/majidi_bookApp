import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import {connect} from 'react-redux';
import {getLogin} from '../../Redux/Actions/LoginAction';
import {getSearch, limit, kapital} from '../../Redux/Actions/SearchAction';

class SearchTab extends React.Component {
  handleQuery = query => {
    if (this.props.timer) {
      clearTimeout(this.props.timer);
    }
    this.setState({query: this.props.queries});
    this.setState({
      query: setTimeout(() => {
        this.props.getSearch(query);
      }, 100),
    });
  };

  componentDidMount() {
    this.props.searchBook;
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.welcome}>
          <Text style={styles.judulPage}>Search Page</Text>
          <Text style={styles.textWelcome}>
            Welcome, {this.props.dataUser.name}{' '}
          </Text>
        </View>
        <View style={styles.search}>
          <TextInput
            ref={input => {
              this.textInput = input;
            }}
            value={this.props.queries}
            placeholder="Search book..."
            onChangeText={this.handleQuery}
            style={styles.inputSearch}
          />
          <TouchableOpacity
            onPress={() => this.textInput.clear()}
            style={styles.icon}>
            <Icon
              name="close-circle"
              size={30}
              color={'#465881'}
              style={styles.iconClose}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.hslsearch}>
          {!this.props.searchBook.length ? (
            <Text></Text>
          ) : (
            this.props.searchBook.map(book => {
              return (
                <TouchableOpacity
                  key={book.id}
                  onPress={() =>
                    this.props.navigation.navigate('BookDetail', book.id)
                  }>
                  <Image
                    source={{uri: book.cover_image}}
                    style={styles.imgBook}
                  />
                  <Text style={styles.judulBook}>
                    {this.props.limit(book.title)}
                  </Text>
                </TouchableOpacity>
              );
            })
          )}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataUser: state.LoginReducer.user,
    searchBook: state.SearchReducer.query,
    queries: state.SearchReducer.queries,
    timer: state.SearchReducer.searchTimer,
  };
};

const mapDispatchToProps = {
  getLogin: getLogin,
  getSearch: getSearch,
  limit: limit,
  kapital: kapital,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchTab);
