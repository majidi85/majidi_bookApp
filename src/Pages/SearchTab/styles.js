import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    judulPage:{
        fontFamily:'Poppins',
        fontSize:24,
        fontWeight:'bold',
        justifyContent:'center',
        alignItems:'center'
    },
    textWelcome:{
        fontFamily:'Poppins',
        fontSize:16,
        fontWeight:'bold'
    },
    welcome:{
        margin:10,
    },
    search:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        padding:20,
    },
    inputSearch:{
        backgroundColor:"#465881",
        borderRadius: 20,
        paddingLeft: 30,
        height:50,
        color:'white',
        fontSize:22,
        width:"90%",
    },
    icon:{
        justifyContent:'center',
    },
    iconClose: {
        
    },
    hslsearch:{
        flex:10,
        padding: 10,
        flexDirection:'row',
        flexWrap:'wrap',
        justifyContent:'flex-start',
        marginTop:10,
    },
    imgBook:{
        height: 190,
        width: 135,
        borderRadius: 15,
        borderColor: 'grey',
        overflow: 'hidden',
        margin: 15,
        resizeMode: 'contain',
    },
    judulBook:{
        color: '#2d3035',
        marginTop: 5,
        marginBottom: 10,
        marginLeft:15,
        justifyContent:'center',
        alignItems:'center'

    },
})


export default styles