import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
} from 'react-native';
import styles from './styles';
import {connect} from 'react-redux';
import {getFavorite} from '../../Redux/Actions/FavoriteAction';
import Skeleton from './skeleton';

class FavoriteTab extends React.Component {

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.props.getFavorite();
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  render() {
    return (
      <ScrollView
        style={styles.container}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={this.props.loading}
            onRefresh={() => this.props.getFavorite()}
          />
        }>
        {this.props.loading ? (
          <Skeleton />
        ) : (
          <>
        <View style={styles.judul}>
          <Text style={styles.textFavorite}>Favorite Books</Text>
        </View>
        <View style={styles.top}>
          {this.props.bookFavorite.map(book => (
            <View style={styles.topContainer}>
              <TouchableOpacity
                key={book._id}
                onPress={() =>
                  this.props.navigation.navigate('BookDetail',book._id)
                }>
                <Image
                  style={styles.imgBook}
                  source={{uri:book.cover_image}}
                />
                <Text style={styles.judulBook}>{book.title}</Text>
                <Text style={styles.author}>{book.author}</Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
        </>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    bookFavorite: state.FavoriteReducer.listFavorites,
    loading: state.FavoriteReducer.isLoading,
  };
};

const mapDispatchToProps = {
  getFavorite: getFavorite,
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteTab);
