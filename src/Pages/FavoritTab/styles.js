import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    judul:{
        margin:10,
        justifyContent:'center',
        alignItems:'center'
    },
    textFavorite:{
        fontFamily:'Poppins',
        fontSize:24,
        fontWeight:'bold',
        justifyContent:'center',
        alignItems:'center'
    },
    top:{
        flexDirection:'row',
        flexWrap:'wrap',
    },
    topContainer:{
        margin:15,
        justifyContent:'center',
        alignItems:'center',
    },
    imgBook:{
        height: 190,
        width: 135,
        borderRadius: 15,
        borderColor: 'grey',
        overflow: 'hidden',
        margin: 15,
        resizeMode: 'contain',
    },
    judulBook:{
        fontSize:16,
        fontWeight:'bold',
        fontFamily:'Poppins'
    },
    author:{
        fontSize:14,
        fontFamily:'Poppins',
        fontWeight:'800'
    },
})


export default styles
