import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import styles from './styles';

const SuccessRegister = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.textHeader}>Registration Completed!</Text>
      </View>
      <View style={styles.body}>

      <Text style={styles.textBody}>We sent email verification to your email</Text>
      </View>
      <View style={styles.footer}>
        <TouchableOpacity
          style={styles.backLogin}
            onPress={()=>{
                navigation.navigate('Login', {});
            }}
        >
          <Text style={styles.loginText}>Back to Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SuccessRegister;
