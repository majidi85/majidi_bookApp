import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
    },
    header:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    textHeader:{
        fontSize:24,
        fontWeight:'bold'
    },
    body:{
        flex:4,
        justifyContent:'center',
        alignItems:'center',
        marginLeft:40,
        marginRight:40
    },
    textBody:{
        marginTop:20,
        fontSize:22,
        textAlign:'center'
    },
    footer:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:40
    },
    backLogin:{
        width:"80%",
        backgroundColor:"#fb5b5a",
        borderRadius:20,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:40,
        marginBottom:10
      },




})

export default styles