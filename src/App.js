import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './Router';
import {Provider} from 'react-redux';
import {bookStore, permanentStore} from './Redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import InternetConnectionAlert from 'react-native-internet-connection-alert'

const App = () => {
  return (
    // <InternetConnectionAlert
    //   onChange={connectionState => {
    //     console.log('Connection State: ', connectionState);
    //   }}>
      <Provider store={bookStore}>
        <PersistGate persistor={permanentStore} loading={null}>
          <NavigationContainer>
            <Router />
          </NavigationContainer>
        </PersistGate>
      </Provider>
    // </InternetConnectionAlert>
  );
};

export default App;
