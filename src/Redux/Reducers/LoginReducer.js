import {
  POST_LOGIN_START,
  POST_LOGIN_SUCCESS,
  POST_LOGIN_FAILED,
} from '../Actions/LoginAction';
  import {
    REGISTER_START,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
  } from '../Actions/RegisterAction';

const initialState = {
  userRegister:{}, //Register_A_4
  user: {},  
  token:'',
  isLoading: false,
  isAuthenticate: false,
  success: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case POST_LOGIN_START:
      return {
        ...state,
        isLoading: true,
        isAuthenticate: false,
      };

    case POST_LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.dataUser,
        token:action.tokenUser,
        isAuthenticate: true,
        success: true,
      };

    case POST_LOGIN_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    //   case LOGOUT:
    //     return {...state, isAuthenticate: false, success: false};

      case REGISTER_START:
        return {...state, isLoading: true, isAuthenticate: false};

      case REGISTER_SUCCESS:
        return {
          ...state,
          isLoading: false,
          userRegister: action.dataRegister,  //Register_A_3
          success: true,
        };

      case REGISTER_FAILED:
        return {...state, isLoading: false};
    default:
      return state;
  }
};
