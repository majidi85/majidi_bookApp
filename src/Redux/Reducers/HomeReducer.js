import {
    GET_BOOK_LIST_FAILED,
    GET_BOOK_LIST_SUCCESS,
    GET_BOOK_LIST_START,
  } from './../Actions/HomeAction';
  
  const initialState = {
    listBook: [],   //Home_A_4 : taruh dalam array
    // bookDetail: {},
    // titlePage: 'Book Page',
    isLoading: false,
  };

  export default (state = initialState, action) => {
  
    switch (action.type) {
      case GET_BOOK_LIST_START:
        return {...state, isLoading: true};
      case GET_BOOK_LIST_FAILED:
        return {...state, isLoading: false};
      case GET_BOOK_LIST_SUCCESS:
        return {...state, isLoading: false, listBook: action.dataListBook};  //Home_A_3 : dataListBook dari Action simpan dalam listBook
      default:
        return state;
    }
  };