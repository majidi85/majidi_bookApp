import {
  GET_FAVORITE_START,
  GET_FAVORITE_SUCCESS,
  GET_FAVORITE_FAILED,
  POST_FAVORITE_START,
  POST_FAVORITE_SUCCESS,
  POST_FAVORITE_FAILED,
} from '../Actions/FavoriteAction';

const initialState = {
  listFavorites: [],
  isLoading: false,
  addFavorites:{},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_FAVORITE_START:
      return {...state, isLoading: true};
    case GET_FAVORITE_FAILED:
      return {...state, isLoading: false};
    case GET_FAVORITE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        listFavorites: action.dataFavorite,
      };
    case POST_FAVORITE_START:
      return {...state, isLoading: true};
    case POST_FAVORITE_FAILED:
      return {...state, isLoading: false};
    case POST_FAVORITE_SUCCESS: {
      return {
        ...state,
        isLoading:false,
        addFavorites:action.addFavorite,
      };
    }
    default:
      return state;
  }
};
