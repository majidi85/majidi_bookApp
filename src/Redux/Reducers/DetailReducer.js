import {
    GET_BOOK_DETAIL_FAILED,
    GET_BOOK_DETAIL_SUCCESS,
    GET_BOOK_DETAIL_START,
  } from '../Actions/DetailAction';
  
  const initialState = { 
    bookDetail: {}, //Detail_A_4 
    isLoading: false,
  };

  export default (state = initialState, action) => {
  
    switch (action.type) {
      case GET_BOOK_DETAIL_START:
        return {...state, isLoading: true};
      case GET_BOOK_DETAIL_FAILED:
        return {...state, isLoading: false};
      case GET_BOOK_DETAIL_SUCCESS:
        return {
          ...state, 
          isLoading: false, 
          bookDetail: action.dataBookDetail.data, //Detail_A_3 
        };  
      default:
        return state;
    }
  };