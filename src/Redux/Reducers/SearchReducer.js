import {
    SEARCH_BOOK_START,
    SEARCH_BOOK_SUCCESS,
    SEARCH_BOOK_FAILED,
  } from '../Actions/SearchAction';
  
  const initialState = {
    bookID: {} ,   
    query: [], 
    input:'',
    searchTimer: null,
    isLoading: false,
  };

  export default (state = initialState, action) => {
  
    switch (action.type) {
      case SEARCH_BOOK_START:
        return {...state, isLoading: true};
      case SEARCH_BOOK_FAILED:
        return {...state, isLoading: false};
      case SEARCH_BOOK_SUCCESS:
        return {
          ...state, 
        //   isLoading: false, 
          query: action.dataSearch.data.results,
        };  
      default:
        return state;
    }
  };