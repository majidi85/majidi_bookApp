import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import HomeReducer from './Reducers/HomeReducer'
import LoginReducer from './Reducers/LoginReducer'
import DetailReducer from './Reducers/DetailReducer'
import FavoriteReducer from './Reducers/FavoriteReducer'
import {createLogger} from 'redux-logger';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';
import SearchReducer from './Reducers/SearchReducer'


const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
  };

const allReducers = combineReducers ({
    HomeReducer,     //Home_A_5 : Reducer disimpan dalam store
    LoginReducer,   //Register_A_5
    DetailReducer,  //Detail_A_5
    FavoriteReducer,
    SearchReducer,
})

const permanentReducer = persistReducer(persistConfig, allReducers);
const logger = createLogger({});

export const bookStore = createStore(
    permanentReducer,
    applyMiddleware(thunk, logger),
  );

export const permanentStore = persistStore(bookStore);

// const bookStore = createStore(allReducers, applyMiddleware(thunk))
// export default bookStore