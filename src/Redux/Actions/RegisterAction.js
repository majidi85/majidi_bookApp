import axios from 'axios';
export const REGISTER_START = 'REGISTER_START';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILED = 'REGISTER_FAILED';
import {Alert} from 'react-native';


export const getRegister = (requestRegister, callbackData) => async dispatch => {
  try {
    dispatch({
      type: REGISTER_START,
    });
    
    const responseRegister = await axios.post(                  //Register_A_1
      'http://code.aldipee.com/api/v1/auth/register',
      requestLogin,
    );
        
    if (responseRegister.status === 201) {
      dispatch({
        type: REGISTER_SUCCESS,
        dataRegister:responseRegister,              //Register_A_2
      });
      
    callbackData({isRegisterSuccess:true})
    }
         
  } catch (error) {
    dispatch({
      type: REGISTER_FAILED,
    });
    console.log(error.message);
    Alert.alert(error.message, error.responseServer);
  }
};
