import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
export const GET_FAVORITE_START = 'GET_FAVORITE_START';
export const GET_FAVORITE_SUCCESS = 'GET_FAVORITE_SUCCESS';
export const GET_FAVORITE_FAILED = 'GET_FAVORITE_FAILED';
export const POST_FAVORITE_START = 'POST_FAVORITE_START';
export const POST_FAVORITE_SUCCESS = 'POST_FAVORITE_SUCCESS';
export const POST_FAVORITE_FAILED = 'POST_FAVORITE_FAILED';

export const getFavorite = () => async dispatch => {
  try {
    dispatch({
      type: GET_FAVORITE_START,
    });
    const value = await AsyncStorage.getItem('token-user');
    const getFavoritefromServer = await axios.get(
      `http://code.aldipee.com/api/v1/books/my-favorite`,
      {headers: {Authorization: `Bearer ${value}`}},
    );
      dispatch({
        type: GET_FAVORITE_SUCCESS,
        dataFavorite: getFavoritefromServer.data,
      });
    }
   catch (error) {
    dispatch({
      type: GET_FAVORITE_FAILED,
    });
  }
};

export const postFavorite = id => async dispatch => {
  try {
    dispatch({
      type: POST_FAVORITE_START,
    });
    const value = await AsyncStorage.getItem('token-user');
    const addPostFavorite = await axios.post(
      `http://code.aldipee.com/api/v1/books/my-favorite`,
      {id},
      {headers: {Authorization: `Bearer ${value}`}},
    );
    if (addPostFavorite.status === 201) {
      dispatch({
        type: POST_FAVORITE_SUCCESS,
        addFavorite:addPostFavorite,
      });
    }
  } catch (error) {
    dispatch({
      type: POST_FAVORITE_FAILED,
    });
  }
};
