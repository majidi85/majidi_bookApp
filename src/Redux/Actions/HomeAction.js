import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
export const GET_BOOK_LIST_START = 'GET_BOOK_LIST_START'
export const GET_BOOK_LIST_SUCCESS = 'GET_BOOK_LIST_SUCCESS'
export const GET_BOOK_LIST_FAILED = 'GET_BOOK_LIST_FAILED'

export const getBookList = () => async dispatch => {
    try {
        dispatch({
            type: GET_BOOK_LIST_START,
        })
        const value =await AsyncStorage.getItem('token-user')
        //fetching awal
        const datafirstBook = await axios.get('http://code.aldipee.com/api/v1/books',
        {headers:{Authorization:`Bearer ${value}`},})
        //fetching lg untuk get more books
        const dataBook = await axios.get(`http://code.aldipee.com/api/v1/books?limit=${datafirstBook.data.totalResults}`,  //Home_A_1 : fetching dari API simpan dalam dataBook
        {headers:{Authorization:`Bearer ${value}`},})
        dispatch({
            type: GET_BOOK_LIST_SUCCESS,
            dataListBook: dataBook.data.results,   //Home_A_2 : data s.d isi object simpan dalam dataListBook
        })
    } catch (error) {
        dispatch({
            type: GET_BOOK_LIST_FAILED,
        })
    }
}
    