import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
export const POST_LOGIN_START = 'POST_LOGIN_START';
export const POST_LOGIN_SUCCESS = 'POST_LOGIN_SUCCESS';
export const POST_LOGIN_FAILED = 'POST_LOGIN_FAILED';
export const SET_DATA_USER = '@@BOOK/SET_DATA_USER';
export const SET_TOKEN_USER = '@@BOOK/SET_TOKEN_USER';
import {Alert} from 'react-native';


export const getLogin = (requestLogin, callbackData) => async dispatch => {
  try {
    dispatch({
      type: POST_LOGIN_START,
    });
    
    const responseLogin = await axios.post(             //Login_A_1
      'http://code.aldipee.com/api/v1/auth/login',
      requestLogin,
    );
    if (responseLogin.status === 200) {      //Login_A_1_jika_benar
      await AsyncStorage.setItem(
        'token-user',
        responseLogin.data.tokens.access.token,     ////Login_A_1_minta_token
      );

    const dataUser = responseLogin.data.user;       //Login_A_2_data_user
    const tokenUser = responseLogin.data.tokens.access.token; //Login_A_2_data_token
    dispatch({
      type: POST_LOGIN_SUCCESS,
      dataUser: dataUser,  //Login_A_2_data_user_dispatch
      tokenUser: tokenUser,  //Login_A_2_data_token_dispatch
    });

    callbackData({isLoginSuccess: true, namaSaya: 'Majidi'});
      }
  } catch (error) {
    dispatch({
      type: POST_LOGIN_FAILED,
    });
    console.log(error.message);
    Alert.alert(error.message, error.responseServer);
  }
};
//// export const logoutAction = history => {
////   return {type: LOGOUT};
//// };
// export const setUserDataToken = (dataUser, tokenUser) => async dispatch => {
//   dispatch({
//     type: SET_DATA_USER,
//   });
//   dispatch({
//     type: SET_TOKEN_USER,
//   });
// };
