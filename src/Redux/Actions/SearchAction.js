import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const SEARCH_BOOK_START = 'SEARCH_BOOK_START';
export const SEARCH_BOOK_SUCCESS = 'SEARCH_BOOK_SUCCESS';
export const SEARCH_BOOK_FAILED = 'SEARCH_BOOK_FAILED';
export const LIMITS_TEXT = 'LIMITS_TEXT';
export const CAPITAL = 'CAPITAL';

export const getSearch = query => async dispatch => {
  try {
    dispatch({
      type: SEARCH_BOOK_START,
    });
    const value = await AsyncStorage.getItem('token-user');
    const serverSearch = await axios.get(
      `http://code.aldipee.com/api/v1/books?title=${query}`,
      {headers: {Authorization: `Bearer ${value}`}},
    );
    dispatch({
      type: SEARCH_BOOK_SUCCESS,
      dataSearch : serverSearch,
    });
  } catch (error) {
    dispatch({
      type: SEARCH_BOOK_FAILED,
    });
  }
};

export const limit = input => dispatch => {
  dispatch({
    type: LIMITS_TEXT,
  });
  if (input.length > 25) {
    return input.substring(0, 25) + '..';
  }
  return input;
};

export const kapital = text => dispatch => {
  dispatch({
    type: CAPITAL,
  });
  if (text.length != 0) {
    const words = text.split(' ');
    const word = words.map(
      x => x[0].toUpperCase() + x.substr(1).toLowerCase(),
    );
    return word.join(' ');
  }
};
