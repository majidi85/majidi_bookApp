import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
export const GET_BOOK_DETAIL_START = 'GET_BOOK_DETAIL_START'
export const GET_BOOK_DETAIL_SUCCESS = 'GET_BOOK_DETAIL_SUCCESS'
export const GET_BOOK_DETAIL_FAILED = 'GET_BOOK_DETAIL_FAILED'

export const getBookDetail = id => async dispatch => {
    try {
        dispatch({
            type: GET_BOOK_DETAIL_START,
        })
        const value =await AsyncStorage.getItem('token-user')
        const serverBookDetail = await axios.get(`http://code.aldipee.com/api/v1/books/${id}`,  //Detail_A_1
        {headers:{Authorization:`Bearer ${value}`},})
        dispatch({
            type: GET_BOOK_DETAIL_SUCCESS,
            dataBookDetail: serverBookDetail, //Detail_A_2 
        })
    } catch (error) {
        dispatch({
            type: GET_BOOK_DETAIL_FAILED,
        })
    }
}
    