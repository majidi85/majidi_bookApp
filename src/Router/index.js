import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import {Splash, Login,Register,SuccessRegister,Home,BookDetail,SearchTab,FavoritTab} from '../Pages';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/Ionicons'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} options={{tabBarIcon:({color, size})=>(<Icon name="home-outline" size={size} color={color}/>)}}/>
      <Tab.Screen name="Search" component={SearchTab} options={{tabBarIcon:({color, size})=>(<Icon name="search-outline" size={size} color={color}/>)}}/>
      <Tab.Screen name="Favorit" component={FavoritTab} options={{tabBarIcon:({color, size})=>(<Icon name="heart-outline" size={size} color={color}/>)}}/>
    </Tab.Navigator>
  )
}

const Router = () => {
    return (
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
          <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
          <Stack.Screen name="Register" component={Register} options={{headerShown:false}}/>
          <Stack.Screen name="SuccessRegister" component={SuccessRegister} options={{headerShown:false}}/>
          <Stack.Screen name="MainApp" component={MainApp} options={{headerShown:false}} />
          <Stack.Screen name="BookDetail" component={BookDetail} options={{headerShown:false}}/>
          {/* <Stack.Screen name="BookDetail" component={BookDetail} options={{headerShown:false}} />  */}
        </Stack.Navigator>
    );
}

export default Router