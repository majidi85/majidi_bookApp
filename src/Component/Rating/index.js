import  Rating from 'react-native-easy-rating';

export default function App() {
  const [rating,setRating] = useState();

  return (
    <Rating
      rating={rating}
      max={5}
      iconWidth={24}
      iconHeight={24}
      onRate={setRating}/>
  );
}
