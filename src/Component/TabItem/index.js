import React from 'react'
import { View, Text,TouchableOpacity } from 'react-native'
import styles from './styles'
import { IconHome, IconHomeActive } from '../../Assets'
import Icon from 'react-native-vector-icons/Ionicons'

const TabItem = ({isFocused, onPress, onLongPress,label}) => {

    const Icon = () => {
        if(label == "Home") return isFocused ? <IconHomeActive /> : <IconHome />
        // if(label == "Search") return isFocused ? <IconSearch /> : <IconSearch />
        // if(label == "Favorit") return isFocused ? <IconFavorit /> : <IconFavorit />
      }

    return (
        <TouchableOpacity
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}
          >
            {/* <Icon /> */}
            <Text style={styles.text(isFocused)}>
              {label}
            </Text>
          </TouchableOpacity>
    )
}

export default TabItem
