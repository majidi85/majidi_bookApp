import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: isFocused => ({
    fontSize: 14,
    color: isFocused ? '#55C895' : '#C8C8C8',
    marginTop: 5,
  }),
});

export default styles;
