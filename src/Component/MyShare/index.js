import Share from 'react-native-share';
import React, { Component } from 'react';
import { View, Button } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'

class MyShare extends Component {
  // onShare = async () => {
  //   try {
  //     const result = await Share.share({
  //       message:
  //         'React Native | A framework for building native apps using React',
  //     });

  //     if (result.action === Share.sharedAction) {
  //       if (result.activityType) {
  //         // shared with activity type of result.activityType
  //       } else {
  //         // shared
  //       }
  //     } else if (result.action === Share.dismissedAction) {
  //       // dismissed
  //     }
  //   } catch (error) {
  //     alert(error.message);
  //   }
  // };

  onShare = async () => {  
    const shareOptions = {
      message: 'This is test sharing',
    }
    try {
      const ShareResponse = Share.open(shareOptions);
    } catch(error) {
      console.log('error sharing : ', error);
    }
  }

  render() {
    return (
      <View>
        <Icon name="share-social-sharp" style={styles.icon} />
      </View>
    );
  }
}

export default MyShare;