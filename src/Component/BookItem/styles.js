import { StyleSheet } from "react-native";

const styles=StyleSheet.create ({
    container:{
        backgroundColor:'#dfebf7',
        margin:5,
    },
    bookitem:{
        height:270,
        width:140
    },
    imgBook:{
        height: 191,
        width: 131,
        borderRadius: 15,
    },
    judul:{
       fontSize:16,
       fontWeight:'bold',
       fontFamily:'Poppins' 
    },
    author:{
        fontSize:12,
        fontFamily:'Poppins',
        fontWeight:'800'
    },
    rating:{
        fontSize:10
    },
})

export default styles