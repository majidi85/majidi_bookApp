import React from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import {getBookList} from '../../Redux/Actions/HomeAction';
import {connect} from 'react-redux';
import {ImageSplash} from '../../Assets/Images';

class BookItem extends React.Component {
  componentDidMount() {
    this.props.getBookList();
  }

  render() {
    // console.log(this.props, 'props doang');
    // console.log(this.props.dataBook, 'databook nih');
    // console.log(this.props.coba_aja, 'coba coba_aja');
    return (
      <TouchableOpacity style={styles.bookitem}
      onPress={()=>{
        this.props.navigation.navigate('BookDetail', {bookID: book.id});    
    }}>
        {/* {this.props.dataBook.length && this.props.dataBook.map(book => ( 
        <View>
          <Image style={styles.imgBook} source={uri:book.cover_image}/>
        </View>
        ))} */}
        {this.props.dataBook.map(book => ( //Home_A_7 : data diolah sesuai keinginan
        <View key={book.id}>
          <Image style={styles.imgBook} source={{uri:`${book.cover_image}`}}/>
          <Text style={styles.judul}>{book.title}</Text>
          <Text style={styles.author}>{book.author}</Text>
          <Text style={styles.rating}>{book.average_rating}</Text>
        </View>
        ))}
      </TouchableOpacity>

      // <View style={styles.container}>
      //  {this.props.dataBook.length && this.props.dataBook.map(book => (    //Home_A_7 //length untuk konfirmasi array
      //  {/* {this.props.dataBook.map(book => ( //Home_A_7 : data diolah sesuai keinginan */}
      //   <View style={styles.desc}>
      //     <Image
      //       style={styles.imgBook}
      //       source={{uri:`${book.cover_image}`}}
      //     />
      //     </View>
      //   <View>
      //       <Text>{book.title}</Text>
      //       <Text>{book.author}</Text>
      //       <Text>{book.average_rating}</Text>
      //       <Text>{book.price}</Text>
      //     </View>

      //    ))}
      // </View>
    );
  }
}

const mapStateToProps = state => {
  //untuk manggil reducer
  return {
    dataBook: state.HomeReducer.listBook, //Home_A_6 : dari store,cari Reducernya dan data yg diinginkan, dlm hal ini listBook, simpan dalam dataBook
    // coba_aja: state.HomeReducer.isLoading,
  };
};

const mapDispatchToProps = {
  //untuk manggil action
  getBookList: getBookList,
};

export default connect(mapStateToProps, mapDispatchToProps)(BookItem);
