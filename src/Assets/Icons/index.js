import IconHome from './home.svg';
import IconHomeActive from './homeActive.svg';

export {
    IconHome,
    IconHomeActive,
}